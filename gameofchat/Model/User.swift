//
//  User.swift
//  gameofchat
//
//  Created by Epnox on 12/04/2018.
//  Copyright © 2018 Epnox. All rights reserved.
//

import UIKit

class User: NSObject {
    @objc var id: String!
    @objc var name: String!
    @objc var email: String!
    @objc var profileImageUrl : String!
    
}
