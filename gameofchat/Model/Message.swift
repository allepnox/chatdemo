//
//  Message.swift
//  gameofchat
//
//  Created by Epnox on 24/04/2018.
//  Copyright © 2018 Epnox. All rights reserved.
//

import UIKit
import Firebase

class Message: NSObject {

    @objc var toId: String?
    @objc var fromId: String?
    @objc var timestamp: NSNumber?
    @objc var text: String?
    
    @objc var imageUrl: String?
    @objc var imageHeight: NSNumber?
    @objc var imageWidth: NSNumber?
    
    @objc var videoUrl: String?
    
    
    func chatPartnerId() -> String? {
        return fromId == Auth.auth().currentUser?.uid ? toId: fromId
    }
    
    init(dictionary: [String : AnyObject]){
        super.init()
        
        toId = dictionary["toId"] as? String
        fromId = dictionary["fromId"] as? String
        timestamp = dictionary["timestamp"] as? NSNumber
        text = dictionary["text"] as? String
        imageUrl = dictionary["imageUrl"] as? String
        imageHeight = dictionary["imageHeight"] as? NSNumber
        imageWidth = dictionary["imageWidth"] as? NSNumber
        videoUrl = dictionary["videoUrl"] as? String
    }
    
}
