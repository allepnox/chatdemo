//
//  Extensions.swift
//  gameofchat
//
//  Created by Epnox on 17/04/2018.
//  Copyright © 2018 Epnox. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView{
    
    func loadImageUsingCacheWithUrlString(urlString: String) {
        
        self.image = nil
        
        //check cache for image first
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
            self.image = cachedImage
            return
        }
        
        //fire off a new download
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            //download hit error so lets return out
            if error != nil{
                print(error!)
                return
            }
            DispatchQueue.main.async(execute: {
                if let downloadImage = UIImage(data: data!){
                    imageCache.setObject(downloadImage, forKey: urlString as AnyObject)
                    self.image = UIImage(data: data!)
                }
            })
        }).resume()
    }
    
}
