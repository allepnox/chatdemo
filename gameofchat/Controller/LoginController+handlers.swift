//
//  LoginController+handlers.swift
//  gameofchat
//
//  Created by Epnox on 13/04/2018.
//  Copyright © 2018 Epnox. All rights reserved.
//

import UIKit
import Firebase

extension LoginController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func handleRegister(){
        
        guard let email = emailTextField.text, let password = passwordTextField.text, let name = nameTextField.text else {
            print("Forms is not valid")
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { (User, error) in
            if error != nil {
                print(error!)
                return
            }
            
            guard let uid = User?.uid else {
                return
            }
            //successfuly authenticated
            let imageName = NSUUID().uuidString
            let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).jpg")
            
            if let profileImage = self.profileImageView.image,let uploadData = UIImageJPEGRepresentation(profileImage, 0.1) {
                
                storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                    if error != nil {
                        print(error!)
                        return
                    }
                    if let profileImageUrl = metadata?.downloadURL()?.absoluteString {
                        let values = ["name": name, "email":email, "profileImageUrl": profileImageUrl]
                        let updated =  metadata?.updated
                        self.registerUserIntoDatabaseWithUid(uid: uid, values: values as [String : AnyObject], updated: (updated as Date?)!)
                    }
                    print(metadata!)
                })
            }
        }
    }
    
    func registerUserIntoDatabaseWithUid(uid : String, values : [String : AnyObject], updated : Date){
        let ref = Database.database().reference()
        let reference = ref.child("pichere-production").child("users").child(uid)

//        let reference = ref.child("pichere-production").child("users").child(uid)
        let usersReference = reference.child("meta")
        let updateRef = reference.child("updated")
        usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
            if err != nil{
                print(err! )
                return
            }
        
            else{
                
                let user = User()
                user.setValuesForKeys(values)
                
                self.messagesController?.setupNavBarWithUser(user: user)
                
                self.dismiss(animated: true, completion: nil)
                print("Save user successfully into firebase db")
            }
        })
        
        updateRef.setValue(Int(updated.timeIntervalSince1970))
    }
    
    
    @objc func handleSelectProfileImageView(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImageFromPicker : UIImage?
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            profileImageView.image = selectedImage 
        }
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("cancel picker")
        dismiss(animated: true, completion: nil)
    }
}
